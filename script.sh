#!/bin/bash
 
docker rm -f {NOME_DO_DOCKER}
docker rmi -f registry.b2w.io/b2wdigital/django_n2_guerreiro
docker pull registry.b2w.io/b2wdigital/django_n2_guerreiro
docker run -d --network host --name registry.b2w.io/b2wdigital/django_n2_guerreiro:latest
